module app.home {
    class HomeController implements app.models.IHomeModel {
        public title: string;
        public description: string;
        public image: string;
        public summaryKeys: string[];
        private teste: string;


        constructor() {
            this.teste = "antonio";
            this.title = "Summary of qualifications";
            this.image = "app/images/edilson.jpg";
            this.summaryKeys = [
                "Asp.Net MVC",
                "MongoDb",
                "AngularJs",
                "TypeScript",
            ];


        }
    }
    angular
        .module("home")
        .controller("HomeController", HomeController);
}
