module app.models {
    export interface IHomeModel {
        title: string;
        image: string;
        description: string;
        summaryKeys: string[];
    }
}
