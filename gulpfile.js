var gulp = require("gulp"),
    browserify = require("browserify"),
    source = require("vinyl-source-stream"),
    buffer = require("vinyl-buffer"),
    tslint = require("gulp-tslint"),
    tsc = require("gulp-typescript"),
    sourcemaps = require("gulp-sourcemaps"),
    uglify = require("gulp-uglify"),
    runSequence = require("run-sequence"),
    mocha = require("gulp-mocha"),
    istanbul = require("gulp-istanbul"),
    gulpLess = require("gulp-less"),
    path = require("path"),
    browserSync = require('browser-sync').create();

var dirName = "src/app/assets/";

gulp.task("lint", function () {

    var config = { formatter: "verbose", emitError: (process.env.CI) ? true : false };

    return gulp.src([
        "src/**/**.ts",
        "test/**/**.test.ts"
    ])
        .pipe(tslint(config))
        .pipe(tslint.report());

});

var tsProject = tsc.createProject("tsconfig.json");
gulp.task("default", ["build-app"]);
gulp.task("build-app", ["lint", "less"], function () {
    return gulp.src([
        "src/**/**.ts",
        "typings/main.d.ts/"
    ])
        .pipe(tsProject())
        .js.pipe(gulp.dest("src/"));
});

gulp.task("less", function () {
    return gulp.src("src/app/assets/less/*.less")
        .pipe(gulpLess())
        .pipe(gulp.dest(path.join(dirName, "css")));

});

gulp.task("bundle", function () {

    var libraryName = "myapp";
    var mainTsFilePath = "src/app/app.js";
    var outputFolder = "dist/";
    var outputFileName = libraryName + ".min.js";

    var bundler = browserify({
        debug: true,
        standalone: libraryName
    });

    return bundler.add(mainTsFilePath)
        .bundle()
        .pipe(source(outputFileName))
        .pipe(buffer())
        .pipe(sourcemaps.init({ loadMaps: true }))
        .pipe(uglify())
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest(outputFolder));
});

gulp.task("watch", ["default"], function () {

    browserSync.init({
        server: "."
    });

    gulp.watch(["src/**/**.ts", "test/**/*.ts"], ["default"]);
    gulp.watch("dist/*.js").on('change', browserSync.reload);
});

gulp.task("istanbul:hook", function () {
    return gulp.src(['src/**/*.js'])
        // Covering files
        .pipe(istanbul())
        // Force `require` to return covered files
        .pipe(istanbul.hookRequire());
});

gulp.task("test", ["istanbul:hook"], function () {
    return gulp.src('test/**/*.test.js')
        .pipe(mocha({ ui: 'bdd' }))
        .pipe(istanbul.writeReports());
});