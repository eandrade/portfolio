var app;
(function (app) {
    var home;
    (function (home) {
        var HomeController = (function () {
            function HomeController() {
                this.teste = "antonio";
                this.title = "Summary of qualifications";
                this.image = "app/images/edilson.jpg";
                this.summaryKeys = [
                    "Asp.Net MVC",
                    "MongoDb",
                    "AngularJs",
                    "TypeScript",
                ];
            }
            return HomeController;
        }());
        angular
            .module("home")
            .controller("HomeController", HomeController);
    })(home = app.home || (app.home = {}));
})(app || (app = {}));
